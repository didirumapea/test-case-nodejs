const express = require('express');
const router = express.Router();
const checkAuth = require('~/middleware/check-auth')

const auth = require('./auth');
const order = require('./order');

router.use(`/auth`, auth);
router.use(`/order`, order);

module.exports = router;