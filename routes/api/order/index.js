const express = require('express');
const router = express.Router();
const db = require('~/database').db; // as const knex = require('knex')(config);
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const checkAuth = require('~/middleware/check-auth')
router.get('/', (req, res) => {
    res.send('We Are In Employees Leaves Route')
})

// GET
router.get('/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        't1.id',
        't1.customer_name',
        't2.name as user_name',
        't1.customer_name as customer_name',
        't3.name as pizza_name',
        't3.price as price',
        't1.quantity',
        't1.status',
    )
        .from('order as t1')
        .innerJoin('customer as t2', 't1.user_id', 't2.id')
        .innerJoin('pizza as t3', 't1.pizza_id', 't3.id')
        // .where(listWhere)
        .orderBy(`t1.${req.params.col_sort}`, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length === 0){
                res.json({
                    success: true,
                    message: "Leaves masih kosong.",
                    count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Success get data leaves",
                    limit: paginator.per_page,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    data: paginator.data,
                });
            }
        });
});

// ADD
router.post('/add', checkAuth, (req, res) => {
    req.body.user_id = req.userData.id
    db('order')
        .insert(req.body)
        .then(data => {
            res.json({
                success: true,
                message: 'Add order success',
                count: data,
                // data: result,
            });
        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: "Add order failed.",
                // count: data.length,
                data: err,
            });
        });
});

module.exports = router;