const express = require('express');
const router = express.Router();
const db = require('~/database').db; // as const knex = require('knex')(config);
const moment = require('moment');
const config = require('~/config')
const setupPaginator = require('knex-paginator');
const checkAuth = require('~/middleware/check-auth')
const jwt = require('jsonwebtoken')
const mailing = require('~/plugins/mailing')
const cryptoRandomString = require('crypto-random-string');

// bcrypt config
const bcrypt = require('bcrypt');
const saltRounds = 10;
setupPaginator(db);

router.post('/login',  (req, res) => {
    db.select(
        'id',
        'name',
        'email',
        'password',
        )
        .from('customer')
        .where('email', req.body.email)
        .then(data => {
            if (data.length > 0){
                bcrypt.compare(req.body.password, data[0].password).then((result) => {
                    if (result){
                        const user = {
                            id: data[0].id,
                            name: data[0].name,
                            email: data[0].email,
                        }
                        jwt.sign(user, config.jwtSecretKey, {expiresIn: '9999 years'}, (err, token) => {
                            console.log(token)
                            // jwt.sign({user}, secretKey, {expiresIn: '30s'}, (err, token) => {
                            user.token = token
                            res.json({
                                success: true,
                                message: "Login sucessful",
                                // count: data.length,
                                data: user
                            });
                        });
                    }else{
                        res.json({
                            success: result,
                            message: "Password salah.",
                            // data: data,
                        })
                    }
                    // console.log(res)
                });
            }else {
                res.json({
                    success: false,
                    message: "Email salah atau belum terdaftar",
                    // data: data,
                })
            }
        })
});

router.post('/register', (req, res) => {
    db.select(
        'id',
        'email',
    )
        .from('customer')
        .where('email', req.body.email)
        // .orderBy('position', sortBy)
        // .paginate(limit, page, true)
        .then(data => {
            // check REF Code IF Exist
            if (data.length === 0){
                bcrypt.genSalt(saltRounds, (err, salt) => {
                    bcrypt.hash(req.body.password, salt, (err, hash) => {
                        // checkRefCode((gen_ref_code) => {
                            // Store hash in your password DB.
                        req.body.password = hash
                        db('customer')
                            .insert(req.body)
                            .then(data2 => {
                                // console.log(data2)
                                // console.log(data[0])
                                // Mock User
                                db.select(
                                    'id',
                                    'email',
                                    'name',
                                )
                                    .from('customer')
                                    .where('id', data2[0])
                                    .then((data3) => {
                                        //console.log(data3)
                                        const user = {
                                            id: data2[0],
                                            email: data3[0].email,
                                            name: data3[0].name,
                                            created_at: data3[0].created_at,
                                        }
                                        jwt.sign(user, config.jwtSecretKey, {expiresIn: '9999 years'}, (err, token) => {
                                            // jwt.sign({user}, secretKey, {expiresIn: '30s'}, (err, token) => {
                                            user.token = token
                                            res.json({
                                                success: true,
                                                message: "Daftar berhasil dan login sukses",
                                                data: user
                                                // token: token
                                            });
                                        });
                                    })
                            });
                    });
                });
            }else {
                res.json({
                    success: false,
                    message: "Email sudah terdaftar",
                    // data: data,
                })
            }
        })

});

router.get('/profile', checkAuth, (req, res) => {
    res.json({
        success: true,
        message: "User Available",
        data: req.userData,
    })
});

router.get('/logout',  (req, res) => {
    res.json({
        success: true,
        message: "User logget out",
    })
});


module.exports = router;