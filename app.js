const express = require('express');
const app = express();
require("@babel/register")({
    presets: ["@babel/preset-env"]
});
require("regenerator-runtime/runtime");
require('module-alias/register');
const apiRoute = require('./routes/api');
const version = 'v1/';
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
const dotenv = require('dotenv');
const morgan = require('morgan')
dotenv.config();

const checkHeader = require('./middleware/check-header')
// const sshtunnel = require('./database/ssh-tunnel-db')

// use it before all route definitions
app.use(cors());
app.use(express.static('/mnt/cdn'));
app.use(express.static('public'));
app.use(morgan('dev'));

app.get('/', function (req, res) {
    res.send('Hello Absenin API')
})

app.get('/absenin-files', function (req, res) {
    res.send('Hello CDN Absenin!')
})

app.get('/api/'+version, function (req, res) {
    res.send('Hello Absenin Api With Version')
})

app.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb',
    parameterLimit: 1000000
}));
app.use(bodyParser.json({
    limit: '50mb'
}));

app.use('/api/'+version, checkHeader, apiRoute); //ex localhost:3002/api/merchant/get/list/1

// catch 404 and forward to error handler
app.use((req, res) => {
    // req.app.locals.layout = false;
    // res.status(404).render('pages/404');
    res.status(404).json({ status: "error", message: "Page is not found" });
});




app.use(cookieParser());
// app.listen('3002')
const PORT = process.env.PORT || process.env.PORT
app.listen(PORT, console.log(`Server started on port ${PORT}`))