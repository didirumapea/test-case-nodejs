// import * as firebase from 'firebase/app'
const firebase = require('firebase');
require('firebase/auth');

if (!firebase.apps.length) {
    firebase.initializeApp({
        apiKey: "AIzaSyDagN56QXgKBd_nhy1SfEbo08BQic2pYhQ",
        authDomain: "raja-repair.firebaseapp.com",
        databaseURL: "https://raja-repair.firebaseio.com",
        projectId: "raja-repair",
        storageBucket: "raja-repair.appspot.com",
        messagingSenderId: "1007596969810",
        appId: "1:1007596969810:web:dcf9630685531def71a48b",
        measurementId: "G-3F6E4RN6PW"
    })
}

module.exports = firebase.database()
