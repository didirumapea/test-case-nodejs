const fetch = require('node-fetch');
const jwt = require('jsonwebtoken');

module.exports = async (req, res, next) => {
    try{
        if (req.headers['on-project'] !== process.env.H_ON_PROJECT){
            next();
            // return res.status(401).json({
            //     // message: 'auth Failed'
            //     message: 'You are not allowed here'
            // })
        }else{
            if (req.headers['signature-key'] !== process.env.H_SIGN_KEY){
                next();
                // return res.status(401).json({
                //     // message: 'auth Failed'
                //     message: 'You are in restristic area.'
                // })
            }else{
                if (req.query['secret-key'] === process.env.SECRET_KEY){
                    next();
                } else {
                    try {
                        const response = await fetch(process.env.URL + 'secret-token.json');
                        const data = await response.json();
                        jwt.verify(process.env.H_SIGN_KEY, data.secret_token);
                        next();
                    } catch(error) {
                        if (error.message === 'jwt expired'){
                            return res.status(401).json({
                                // message: 'auth Failed'
                                message: 'Auth Token Expired'
                            })
                        } else {
                            return res.status(401).json({
                                // message: 'auth Failed'
                                message: 'Auth Token Failed'
                            })
                        }
                        // err
                    }
                }
            }
        }
        // console.log(req.headers['on-project'])
        // const token = req.headers.authorization.split(' ')[1]
        // req.userData = jwt.verify(token, config.jwtSecretKey);
    }catch (error) {
        let err;
        if (error.message === 'jwt expired'){
            err = 'Token Expired'
        }else{
            err = 'Auth Header Failed'
        }
        return res.status(401).json({
            // message: 'auth Failed'
            message: err
        })
    }
}