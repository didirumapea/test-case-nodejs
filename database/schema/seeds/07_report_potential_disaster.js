const date = require('../plugins/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('report_potential_disaster').del()
        .then(function () {
            // Inserts seed entries
            return knex('report_potential_disaster').insert([
                {
                    id: 1,
                    user_id: 1,
                    organization_id: 1,
                    disaster_id: 1,
                    potential_unique_id: 'dsa9s9tmb1',
                    karakteristik_ancaman: 'This is First Title report disaster org',
                    kerentanan: 'this-is-first-title-report-disaster-org',
                    kapasitas: 'This is First Desc',
                    phone: 'Jati Nangor',
                    contact_name: 'Jati Nangor',
                    contact_phone: 'Jati Nangor',
                    contact_detail: 'Jati Nangor',
                    coordinat: '{"lat": "123.109392109321", "lng": "19.231929058912"}',
                    potential_disaster_radius: 5000,
                    publish_date: '2020-03-15 19:34:16',
                },
                {
                    id: 2,
                    user_id: 2,
                    organization_id: 2,
                    disaster_id: 3,
                    potential_unique_id: 'DSJ92LG8W5',
                    karakteristik_ancaman: 'This is First Title report disaster org',
                    kerentanan: 'this-is-first-title-report-disaster-org',
                    kapasitas: 'This is First Desc',
                    phone: 'Jati Nangor',
                    contact_name: 'Jati Bening',
                    contact_phone: '+62813219898210',
                    contact_detail: 'Jati Nangor',
                    coordinat: '{"lat": "123.109392109321", "lng": "19.231929058912"}',
                    potential_disaster_radius: 2245,
                    publish_date: '2020-03-15 19:34:16',
                },

            ]);
        }).catch((err) => {
            console.log(err)
        });
};
