const date = require('../plugins/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('master_account_type').del()
        .then(function () {
            // Inserts seed entries
            return knex('master_account_type').insert([
                {
                    id: 1,
                    name: 'General',
                    slug: 'general',
                },
                {
                    id: 2,
                    name: 'Organization',
                    slug: 'organization',
                },
                {
                    id: 3,
                    name: 'Goverment',
                    slug: 'goverment',
                },
                {
                    id: 4,
                    name: 'Banned',
                    slug: 'banned',
                },
                {
                    id: 5,
                    name: 'Admin Bumikita',
                    slug: 'admin-bumikita',
                },

            ]);
        }).catch((err) => {
            console.log(err)
        });
};
